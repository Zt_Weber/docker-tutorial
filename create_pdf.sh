#
# File Name: create_pdf.sh
# Created By: Zach Weber
# Created On: 2019-05-24

# variables 
imageName="rmd2pdf"
imageDir="rmd2pdf/"
hostVolume=`pwd`"/testdir/"
containerVolume="/testdir/"

# build docker image 
sudo docker build -t ${imageName} ${imageDir} 

# run docker image 
sudo docker run -itd --name ${imageName} \
    -v ${hostVolume}:${containerVolume} \
    ${imageName}

# execute script
sudo docker exec rmd2pdf R -e "Sys.setenv(RSTUDIO_PANDOC='/usr/bin/pandoc');
                            setwd('/testdir/');library(rmarkdown);library(utils);
                            render('${1}', 'pdf_document')" 

# stop container
sudo docker stop ${imageName}

# cleanup system
sudo docker system prune -f

